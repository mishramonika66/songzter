@extends('front.layout.app')

@section('title', 'Home | songzter')
<!--/ in case you want to write JS, write here/-->
@section('custom_js')
@parent
<!--/ in case you want to write JS, write here/-->


<!--/ in case you want to write JS, write here/-->
@endsection


@section('custom_css')
@parent
<!--/ in case you want to write CSS, write here/-->




<!--/ in case you want to write CSS, write here/-->
@endsection
@section('sidebar')
@parent
<!--/ in case you want add something to Sidebar, write here/-->



<!--/ in case you want add something to Sidebar, write here/-->
@endsection

@section('content')



<!--=================================
 banner -->
<section id="home-slider" class="fullscreen">
  <div class="banner bg-1 d-flex bg-overlay-red align-items-center" style="background-image: url('{{ url('/public/assets') }}/images/bg/bg-2.jpg');" >
      <div class="container">
        <div class="row text-center">
          <div class="col-md-12 text-white">
            <h1 class="animated3 text-white mb-20">Welcome <span>to</span></h1>
            <h2 class="textrotate"><span class="rotate"> Songzter, Songzter, Songzter</span></h2>
          </div>
        </div>
    </div>
  </div>
</section>

<!--=================================
 banner --> 




<!--=================================
 Page Section -->
<section class="page-section-ptb">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8 text-center mb-5">
        <h2 class="title divider-2 mb-3">Our History</h2>
        <p class="mb-0 lead">Eum cu tantas legere complectitur, hinc utamur ea eam. Eum patrioque mnesarchum eu, diam erant convenire et vis. Et essent evertitur sea, vis cu ubique referrentur, </p>
      </div>
      </div>
      <div class="row">
      <div class="col-md-6 align-self-center mb-3"><img class="img-fluid" src="{{ url('/public/assets') }}/images/about/01.jpg" alt="" /></div>
      <div class="col-md-6">
       
        <h5 class="clearfix text-orange mb-2">A World Of Infinite Opportunities</h5>
        <p class="mb-0">Eum cu tantas legere complectitur, hinc utamur ea eam. Eum patrioque mnesarchum eu, diam erant convenire et vis. Et essent evertitur sea, vis cu ubique referrentur, sed eu dicant expetendis.Eum cu tantas legere complectitur, hinc utamur ea eam. Eum patrioque mnesarchum eu, diam erant convenire et vis. Et essent evertitur sea, vis cu ubique referrentur, sed eu dicant expetendis. <br/>
          <br/>
          Eum cu tantas legere complectitur, hinc utamur ea eam. Eum patrioque mnesarchum eu, diam erant convenire et vis. Et essent evertitur sea, vis cu ubique referrentur, sed eu dicant expetendis.Eum cu tantas legere complectitur, hinc utamur ea eam. Eum patrioque mnesarchum eu, diam erant convenire et vis. Et essent evertitur sea, vis cu ubique referrentur, sed eu dicant expetendis.Eum cu tantas legere </p>
      </div>
    </div>
  </div>
</section>

<!--=================================

Counter -->

<section class="page-section-pb">
  <div class="container">
    <div class="row">
      <div class="col-md-3 d-flex">
        <div class="counter left_pos"> <i class="glyph-icon flaticon-people-2"></i> <span class="timer" data-to="1600" data-speed="10000">1600</span>
          <label class="lead muted">Total Members</label>
        </div>
      </div>
      <div class="col-md-3 d-flex">
        <div class="counter left_pos"> <i class="glyph-icon flaticon-favorite"></i> <span class="timer" data-to="750" data-speed="10000">750</span>
          <label class="lead muted">Online Members</label>
        </div>
      </div>
      <div class="col-md-3 d-flex">
        <div class="counter left_pos"> <i class="glyph-icon flaticon-charity"></i> <span class="timer" data-to="380" data-speed="10000">380</span>
          <label class="lead muted">Albums</label>
        </div>
      </div>
      <div class="col-md-3 d-flex">
        <div class="counter left_pos"> <i class="glyph-icon flaticon-candelabra"></i> <span class="timer" data-to="370" data-speed="10000">370</span>
          <label class="lead muted">Artists</label>
        </div>
      </div>
    </div>
  </div>
</section>

<!--=================================
Background -->

<section class="page-section-ptb text-white bg-overlay-black-70 bg text-center" style="background:url('{{ url('/public/assets') }}/images/bg/bg-7.jpg') no-repeat 0 0; background-size: cover;">
  <div class="container">
    <div class="row justify-content-center mb-3">
      <div class="col-md-10">
        <h2 class="title divider-2 mb-3">Hard Rock Music</h2>
        <p class="lead">Eum cu tantas legere complectitur, hinc utamur ea eam. Eum patrioque mnesarchum eu, diam erant convenire et vis. Et essent evertitur sea, vis cu ubique referrentur, sed eu dicant expetendis.</p>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <h5 class="title mb-3">see video</h5>
        <div class="popup-gallery"> <a href="https://youtu.be/8xg3vE8Ie_E" class="play-btn popup-youtube"> <span><i class="glyph-icon flaticon-play-button"></i></span></a> </div>
      </div>
    </div>
  </div>
</section>

<!--=================================
Last added profile -->

<section class="page-section-ptb profile-slider gray-bg">
  <div class="container">
    <div class="row justify-content-center mb-5 sm-mb-3">
      <div class="col-md-8 text-center">
        <h2 class="title divider-2">Featured Artists</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-3 sm-mb-3"><div class="profile-item-hover"> <a href="profile-details.html" class="profile-item">
        <div class="profile-image clearfix"><img class="img-fluid" src="{{ url('/public/assets') }}/images/profile/01.png" alt="" /></div>
        <div class="profile-details profile-text">
          <h5 class="title">Jimmy Perez</h5>
          <span class="text-black">23 Years Old</span> </div>
        </a>
        </div> </div>
      <div class="col-md-3 sm-mb-3"><div class="profile-item-hover"> <a href="profile-details.html" class="profile-item">
        <div class="profile-image clearfix"><img class="img-fluid" src="{{ url('/public/assets') }}/images/profile/02.png" alt="" /></div>
        <div class="profile-details profile-text">
          <h5 class="title">Curtis Massey</h5>
          <span class="text-black">21 Years Old</span> </div>
        </a></div> </div>
      <div class="col-md-3 sm-mb-3"><div class="profile-item-hover"> <a href="profile-details.html" class="profile-item">
        <div class="profile-image clearfix"><img class="img-fluid" src="{{ url('/public/assets') }}/images/profile/03.png" alt="" /></div>
        <div class="profile-details profile-text">
          <h5 class="title">Mike Carter</h5>
          <span class="text-black">19 Years Old</span> </div>
        </a></div> </div>
      <div class="col-md-3"><div class="profile-item-hover"> <a href="profile-details.html" class="profile-item">
        <div class="profile-image clearfix"><img class="img-fluid" src="{{ url('/public/assets') }}/images/profile/04.png" alt="" /></div>
        <div class="profile-details profile-text">
          <h5 class="title">Cecelia Torres</h5>
          <span class="text-black">20 Years Old</span> </div>
        </a></div> </div>
    </div>
  </div>
</section>

<!--=================================
they found true love -->

<section class="page-section-ptb text-center">
  <div class="container">
    <div class="row justify-content-center mb-5 sm-mb-3">
      <div class="col-md-8">
        <h2 class="title divider-2 mb-3">How Songzter Works</h2>
        <p class="lead mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna</p>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4 sm-mb-3">
        <div class="timeline-badge mb-2 time-icoms"><i class="fa fa-check-circle" aria-hidden="true"></i></div>
        <h4 class="title divider-3 mb-3">Premium Content</h4>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  enim ad minim veniam,            quis</p>
      </div>
      <div class="col-md-4 sm-mb-3">
        <div class="timeline-badge mb-2 time-icoms"><i class="fa fa-usd" aria-hidden="true"></i></div>
        <h4 class="title divider-3 mb-3">Easy Payment Options</h4>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  enim ad minim veniam,            quis</p>
      </div>
      <div class="col-md-4">
        <div class="timeline-badge mb-2 time-icoms"><i class="fa fa-music" aria-hidden="true"></i></div>
        <h4 class="title divider-3 mb-3">Join the Songzter</h4>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  enim ad minim veniam,            quis</p>
      </div>
    </div>
  </div>
</section>

<!--=================================
Counter -->

<section class="page-section-ptb pb-0 text-white text-center our-app our-app-2 position-relative overflow-h" style="background: url('{{ url('/public/assets') }}/images/bg/intro-slide3.jpg) no-repeat 0 0; background-size: cover;">
  <div class="timeline-inner">
    <div class="container-fluid">
      
      <div class="row mt-5 sm-mt-3">
        <div class="col-md-12">
          <h2 class="title mb-3">Want to join Songzter? </h2>
      
            <p class="lead mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod <br/>tempor incididunt ut labore et dolore magna</p>
			   <a class="button btn-lg full-rounded white-bg text-dark"><span> Sign Up</span></a>
        </div>
      </div>
    </div>
  </div>
</section>

<!--=================================
Post Style -->

<section class="page-section-ptb grey-bg">
  <div class="container">
    <div class="row justify-content-center mb-5 sm-mb-3">
      <div class="col-md-8 text-center">
        <h2 class="title divider-2 mb-3">Our Recent Blogs</h2>
        <p class="lead mb-0">Nulla quis lorem ut libero malesuada feugiat. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Quisque velit nisi, pretium ut lacinia in, elementum id enim.</p>
      </div>
    </div>
    <div class="row post-article">
      <div class="col-md-4">
        <div class="post post-artical top-pos">
          <div class="post-image clearfix"><img class="img-fluid" src="{{ url('/public/assets') }}/images/blog/blog1.png" alt="" /></div>
          <div class="post-details">
            <div class="post-date">27<span class="text-black">MAR</span></div>
            <div class="post-meta"> <a href="#"><i class="fa fa-user"></i> Admin</a> <a href="#"><i aria-hidden="true" class="fa fa-heart-o"></i>98 Like</a> <a href="#"><i class="fa fa-comments-o last-child"></i>Comments</a> </div>
            <div class="post-title">
              <h5 class="title text-uppercase"><a href="blog-singal-fullwidth.html">Celebrating Winter Holidays</a></h5>
            </div>
            <div class="post-content">
              <p>Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Pellentesque in ipsum id orci porta dapibus. Curabitur aliquet quam..</p>
            </div>
            <a class="button" href="blog-singal-fullwidth.html">read more..</a> </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="post post-artical top-pos">
          <div class="post-image clearfix"><img class="img-fluid" src="{{ url('/public/assets') }}/images/blog/blog2.png" alt="" /></div>
          <div class="post-details">
            <div class="post-date">15<span class="text-black">MAR</span></div>
            <div class="post-meta"> <a href="#"><i class="fa fa-user"></i> Admin</a> <a href="#"><i aria-hidden="true" class="fa fa-heart-o"></i>98 Like</a> <a href="#"><i class="fa fa-comments-o last-child"></i>Comments</a> </div>
            <div class="post-title">
              <h5 class="title text-uppercase"><a href="blog-singal-fullwidth.html">Dance with DJ Nowan Clubs</a></h5>
            </div>
            <div class="post-content">
              <p>Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Pellentesque in ipsum id orci porta dapibus. Curabitur aliquet quam..</p>
            </div>url
            <a class="button" href="blog-singal-fullwidth.html">read more..</a> </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="post post-artical top-pos sm-mb-0">
          <div class="post-image clearfix"><img class="img-fluid" src="{{ url('/public/assets') }}/images/blog/blog3.png" alt="" /></div>
          <div class="post-details">
            <div class="post-date">08<span class="text-black">MAR</span></div>
            <div class="post-meta"> <a href="#"><i class="fa fa-user"></i> Admin</a> <a href="#"><i aria-hidden="true" class="fa fa-heart-o"></i>98 Like</a> <a href="#"><i class="fa fa-comments-o last-child"></i>Comments</a> </div>
            <div class="post-title">
              <h5 class="title text-uppercase"><a href="blog-singal-fullwidth.html">Electus dolorum facere illo </a></h5>
            </div>
            <div class="post-content">
              <p>Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Pellentesque in ipsum id orci porta dapibus. Curabitur aliquet quam..</p>
            </div>
            <a class="button" href="blog-singal-fullwidth.html">read more..</a> </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!--=================================
Testimonial-->

<section class="page-section-ptb pb-130 sm-pb-6 dark-bg bg fixed bg-overlay-black-80 text-white" style="background-image:url('{{ url('/public/assets') }}/images/bg/bg-3.jpg);">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8 text-center">
        <h2 class="title divider-2">Testimonials</h2>
      </div>
    </div>
    <div class="row justify-content-center">
      <div class="col-md-10">
        <div class="owl-carousel" data-nav-dots="true" data-items="1" data-md-items="1" data-sm-items="1">
          <div class="item">
            <div class="testimonial clean">
              <div class="testimonial-avatar"> <img alt="" src="{{ url('/public/assets') }}/images/thumbnail/thum-1.jpg"> </div>
              <div class="testimonial-info"> Our old site was very information-heavy; the Constro helped to capture and simplify the message we wanted to get across. It’s now more precise, easy to use and looks great! </div>
              <div class="author-info"> <strong>Jack Thompson - <span>Usa</span></strong> </div>
            </div>
          </div>
          <div class="item">
            <div class="testimonial clean">
              <div class="testimonial-avatar"> <img alt="" src="{{ url('/public/assets') }}/images/thumbnail/thum-2.jpg"> </div>
              <div class="testimonial-info"> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis </div>
              <div class="author-info"> <strong>Miss Jorina Akter - <span>Iraq</span></strong> </div>
            </div>
          </div>
          <div class="item">
            <div class="testimonial clean">
              <div class="testimonial-avatar"> <img alt="" src="{{ url('/public/assets') }}/images/thumbnail/thum-3.jpg"> </div>
              <div class="testimonial-info"> Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam </div>
              <div class="author-info"> <strong>Adam Cooper - <span> New york</span></strong> </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="page-section-ptb pb-130 sm-pb-6 grey-bg story-slider">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8 text-center">
        <h2 class="title divider-2">Latest Media</h2>
      </div>
    </div>
  </div>
  <div class="owl-carousel" data-nav-dots="true" data-items="5" data-md-items="4" data-sm-items="2" data-xx-items="1" data-space="30">
    <div class="item">
      <div class="story-item">
        <div class="story-image clearfix"><img class="img-fluid" src="{{ url('/public/assets') }}/images/story/01.jpg" alt="" />
          <div class="story-link"><a href="stories-details.html"><i class="glyph-icon flaticon-add"></i></a></div>
        </div>
        <div class="story-details text-center">
          <div class="about-des">Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in</div>
        </div>
        <div class="yellow-bg story-text py-3 text-white text-center">
          <h5 class="title text-uppercase">Quinnel &amp; Jonet</h5>
        </div>
      </div>
    </div>
    <div class="item">
      <div class="story-item">
        <div class="story-image clearfix"><img class="img-fluid" src="{{ url('/public/assets') }}/images/story/02.jpg" alt="" />
          <div class="story-link"><a href="stories-details.html"><i class="glyph-icon flaticon-add"></i></a></div>
        </div>
        <div class="story-details text-center">
          <div class="about-des">Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in</div>
        </div>
        <div class="yellow-bg story-text py-3 text-white text-center">
          <h5 class="title text-uppercase">Adam &amp; Eve</h5>
        </div>
      </div>
    </div>
    <div class="item">
      <div class="story-item">
        <div class="story-image clearfix"><img class="img-fluid" src="{{ url('/public/assets') }}/images/story/03.jpg" alt="" />
          <div class="story-link"><a href="stories-details.html"><i class="glyph-icon flaticon-add"></i></a></div>
        </div>
        <div class="story-details text-center">
          <div class="about-des">Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in</div>
        </div>
        <div class="yellow-bg story-text py-3 text-white text-center">
          <h5 class="title text-uppercase">Bella &amp; Edward</h5>
        </div>
      </div>
    </div>
    <div class="item">
      <div class="story-item">
        <div class="story-image clearfix"><img class="img-fluid" src="{{ url('/public/assets') }}/images/story/04.jpg" alt="" />
          <div class="story-link"><a href="stories-details.html"><i class="glyph-icon flaticon-add"></i></a></div>
        </div>
        <div class="story-details text-center">
          <div class="about-des">Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in</div>
        </div>
        <div class="yellow-bg story-text py-3 text-white text-center">
          <h5 class="title text-uppercase">DEMI &amp; HEAVEN</h5>
        </div>
      </div>
    </div>
    <div class="item">
      <div class="story-item">
        <div class="story-image clearfix"><img class="img-fluid" src="{{ url('/public/assets') }}/images/story/05.jpg" alt="" />
          <div class="story-link"><a href="stories-details.html"><i class="glyph-icon flaticon-add"></i></a></div>
        </div>
        <div class="story-details text-center">
          <div class="about-des">Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in</div>
        </div>
        <div class="yellow-bg story-text py-3 text-white text-center">
          <h5 class="title text-uppercase">David &amp; Bathsheba</h5>
        </div>
      </div>
    </div>
    <div class="item">
      <div class="story-item">
        <div class="story-image clearfix"><img class="img-fluid" src="{{ url('/public/assets') }}/images/story/06.jpg" alt="" />
          <div class="story-link"><a href="stories-details.html"><i class="glyph-icon flaticon-add"></i></a></div>
        </div>
        <div class="story-details text-center">
          <div class="about-des">Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in</div>
        </div>
        <div class="yellow-bg story-text py-3 text-white text-center">
          <h5 class="title text-uppercase">Eros &amp; Psychi</h5>
        </div>
      </div>
    </div>
    <div class="item">
      <div class="story-item">
        <div class="story-image clearfix"><img class="img-fluid" src="{{ url('/public/assets') }}/images/story/07.jpg" alt="" />
          <div class="story-link"><a href="stories-details.html"><i class="glyph-icon flaticon-add"></i></a></div>
        </div>
        <div class="story-details text-center">
          <div class="about-des">Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in</div>
        </div>
        <div class="yellow-bg story-text py-3 text-white text-center">
          <h5 class="title text-uppercase">Hector &amp; Andromache</h5>
        </div>
      </div>
    </div>
    <div class="item">
      <div class="story-item">
        <div class="story-image clearfix"><img class="img-fluid" src="{{ url('/public/assets') }}/images/story/01.jpg" alt="" />
          <div class="story-link"><a href="stories-details.html"><i class="glyph-icon flaticon-add"></i></a></div>
        </div>
        <div class="story-details text-center">
          <div class="about-des">Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in</div>
        </div>
        <div class="yellow-bg story-text py-3 text-white text-center">
          <h5 class="title text-uppercase">Bonnie &amp; Clyde</h5>
        </div>
      </div>
    </div>
    <div class="item">
      <div class="story-item">
        <div class="story-image clearfix"><img class="img-fluid" src="{{ url('/public/assets') }}/images/story/02.jpg" alt="" />
          <div class="story-link"><a href="stories-details.html"><i class="glyph-icon flaticon-add"></i></a></div>
        </div>
        <div class="story-details text-center">
          <div class="about-des">Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in</div>
        </div>
        <div class="yellow-bg story-text py-3 text-white text-center">
          <h5 class="title text-uppercase">Henry &amp; Clare</h5>
        </div>
      </div>
    </div>
    <div class="item">
      <div class="story-item">
        <div class="story-image clearfix"><img class="img-fluid" src="{{ url('/public/assets') }}/images/story/03.jpg" alt="" />
          <div class="story-link"><a href="stories-details.html"><i class="glyph-icon flaticon-add"></i></a></div>
        </div>
        <div class="story-details text-center">
          <div class="about-des">Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in</div>
        </div>
        <div class="yellow-bg story-text py-3 text-white text-center">
          <h5 class="title text-uppercase">Casanova &amp; Francesca</h5>
        </div>
      </div>
    </div>
    <div class="item">
      <div class="story-item">
        <div class="story-image clearfix"><img class="img-fluid" src="{{ url('/public/assets') }}/images/story/04.jpg" alt="" />
          <div class="story-link"><a href="stories-details.html"><i class="glyph-icon flaticon-add"></i></a></div>
        </div>
        <div class="story-details text-center">
          <div class="about-des">Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in</div>
        </div>
        <div class="yellow-bg story-text py-3 text-white text-center">
          <h5 class="title text-uppercase">Jack &amp; Sally</h5>
        </div>
      </div>
    </div>
    <div class="item">
      <div class="story-item">
        <div class="story-image clearfix"><img class="img-fluid" src="{{ url('/public/assets') }}/images/story/05.jpg" alt="" />
          <div class="story-link"><a href="stories-details.html"><i class="glyph-icon flaticon-add"></i></a></div>
        </div>
        <div class="story-details text-center">
          <div class="about-des">Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in</div>
        </div>
        <div class="yellow-bg story-text py-3 text-white text-center">
          <h5 class="title text-uppercase">James &amp; Lilly</h5>
        </div>
      </div>
    </div>
  </div>
</section>




@endsection