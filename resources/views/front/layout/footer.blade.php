<footer class="page-section-pt text-white text-center" style="background: url('{{ url('/public/assets') }}/images/pattern/04.png') no-repeat 0 0; background-size: cover;">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-10">
        <div class="row mb-5">
          <div class="col-md-12">
            <h2 class="title divider mb-3">Contact Us</h2>
            <p class="lead mb-0">Eum cu tantas legere complectitur, hinc utamur ea eam. Eum patrioque mnesarchum eu, diam erant convenire et vis. Et essent evertitur sea, vis cu ubique referrentur, sed eu dicant expetendis. Eum cu</p>
          </div>
        </div>
        <div class="row mb-5 sm-mb-2">
          <div class="col-md-4">
            <div class="address-block"> <i class="fa fa-desktop" aria-hidden="true"></i> <a href="mailto:somemail@mail.com">somemail@mail.com</a> </div>
          </div>
          <div class="col-md-4">
            <div class="address-block"> <i class="fa fa-home" aria-hidden="true"></i>
              <address>
              T317 Timber Oak Drive<br/>
              Sundown, TX 79372
              </address>
            </div>
          </div>
          <div class="col-md-4">
            <div class="address-block"> <i class="fa fa-phone" aria-hidden="true"></i> <a href="tel:+000123456789">+000 - 123 - 456 - 789</a> </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 mb-3">
            <h4 class="title divider-3">We Love Talking</h4>
          </div>
          </div>
          <div class="row">
          <div class="col-md-12">
            <div id="formmessage" style="display:none">Success/Error Message Goes Here</div>
            <form id="contactform" class="main-form" method="post" action="php/contact-form.php">
              <div class="form-group half-group">
                <div class="input-group">
                  <input id="name" placeholder="Your name here" class="form-control" name="name" type="text">
                </div>
              </div>
              <div class="form-group half-group">
                <div class="input-group">
                  <input placeholder="Your mail here" class="form-control" name="email" type="email">
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <textarea class="form-control input-message" placeholder="Your message here*" rows="7" name="message"></textarea>
                </div>
              </div>
              <div class="form-group sm-mb-0">
                <input type="hidden" name="action" value="sendEmail"/>
                <button id="submit" name="submit" type="submit" value="Send" class="button btn-lg btn-theme full-rounded animated right-icn"><span>Submit Now<i class="glyph-icon flaticon-hearts" aria-hidden="true"></i></span></button>
              </div>
            </form>
            <div id="ajaxloader" style="display:none"><img class="center-block" src="{{ url('/public/assets') }}/images/loading.gif" alt="" /></div>
          </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="footer-widget mt-5 sm-mt-3">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-8">
          <div class="footer-logo mb-2"> <img class="img-center" src="{{ url('/public/assets') }}/images/footer-logo.png" alt="" /> </div>
          <div class="social-icons color-hover">
            <ul>
              <li class="social-facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
              <li class="social-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
              <li class="social-dribbble"><a href="#"><i class="fa fa-dribbble"></i></a></li>
              <li class="social-gplus"><a href="#"><i class="fa fa-google-plus"></i></a></li>
              <li class="social-youtube"><a href="#"><i class="fa fa-youtube"></i></a></li>
            </ul>
          </div>
          <p class="text-white">© 2020  - Songzter All Right Reserved </p>
        </div>
      </div>
    </div>
  </div>
</footer>
<div id="back-to-top"><a class="top arrow" href="#top"><i class="fa fa-level-up"></i></a></div>
