<!doctype html>
<html lang="en">
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="author" content="Monika Mishra">
        <meta name="generator" content="">
        <title>{{ env('APP_NAME') }} - @yield('title')</title>
        <meta name="description" content="{{ env('APP_NAME') }}"/>
        <meta name="keywords" content="{{ env('APP_NAME') }}"/>
        <!-- twitter card starts from here, if you don't need remove this section -->
        <!-- <meta name="twitter:card" content=""/> -->
        <meta name="twitter:site" content="{{ env('APP_NAME') }}"/>
        <meta name="twitter:url" content="{{ url('/') }}"/>
        <meta name="twitter:title" content="{{ env('APP_NAME') }}"/>
        <!-- maximum 140 char -->

        <meta name="twitter:description" content="{{ env('APP_NAME') }}"/>

        <!-- maximum 140 char -->

        <meta name="twitter:image" content=""/>

        <!-- when you post this page url in twitter , this image will be shown -->
        <!-- twitter card ends from here -->

        <!-- facebook open graph starts from here, if you don't need then delete open graph related  -->

        <meta property="og:title" content="{{ env('APP_NAME') }}"/>
        <meta property="og:url" content="{{ url('/') }}"/>
        <meta property="og:locale" content="en_IN"/>
        <meta property="og:site_name" content="{{ env('APP_NAME') }}"/>

        <!--meta property="fb:admins" content="" /-->  <!-- use this if you have  -->

        <meta property="og:type" content="website"/>
        <meta property="og:image" content=""/>

        <!-- when you post this page url in facebook , this image will be shown -->
        <!-- facebook open graph ends from here -->

        <link rel="canonical" href="{{ url('/') }}">

        <link rel="shortcut icon" href="{{ url('/public/assets') }}/images/favicon.ico" />

        <!-- bootstrap -->
        <link href="{{ url('/public/assets') }}/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- mega menu -->
        <link href="{{ url('/public/assets') }}/css/mega-menu/mega_menu.css" rel="stylesheet" type="text/css" />

        <!-- font awesome -->
        <link href="{{ url('/public/assets') }}/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

        <!-- Flaticon -->
        <link href="{{ url('/public/assets') }}/css/flaticon.css" rel="stylesheet" type="text/css" />

        <!-- Magnific popup -->
        <link href="{{ url('/public/assets') }}/css/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css" />

        <!-- owl-carousel -->
        <link href="{{ url('/public/assets') }}/css/owl-carousel/owl.carousel.css" rel="stylesheet" type="text/css" />

        <!-- General style -->
        <link href="{{ url('/public/assets') }}/css/general.css" rel="stylesheet" type="text/css" />

        <!-- main style -->
        <link href="{{ url('/public/assets') }}/css/style.css" rel="stylesheet" type="text/css" />

        <!-- Style customizer -->
        <link rel="stylesheet" type="text/css" href="{{ url('/public/assets') }}/css/skins/skin-default.css" data-style="styles" />
        <link rel="stylesheet" type="text/css" href="{{ url('/public/assets') }}/css/style-customizer.css" />
        <style>


        </style>
    </head>
    <body>
        <!-- Loader -->

        <div id="wrapper">

            @include('front.layout.header')


            <!-- header -->
            <div class="banner new-block">
                <!--/ 
                
                
                View  
                
                
                
                /-->
                @yield('content')
                <!--/ 
                
                
                View  
                
                
                
                /-->
            </div>


            @include('front.layout.sidenav')


            <script>
                function openNav() {
                    document.getElementById("mySidenav").style.width = "250px";
                }

                /* Set the width of the side navigation to 0 */
                function closeNav() {
                    document.getElementById("mySidenav").style.width = "0";
                }
            </script>


            <!-- Include jQuery -->
            <script type="text/javascript" src="{{ url('/public/assets') }}/js/jquery.min.js"></script> 
            <script type="text/javascript" src="{{ url('/public/assets') }}/js/popper.min.js"></script> 

            <!-- bootstrap --> 
            <script type="text/javascript" src="{{ url('/public/assets') }}/js/bootstrap.min.js"></script> 
            <script type="text/javascript" src="{{ url('/public/assets') }}/js/bootstrap-select.min.js"></script> 

            <!-- appear --> 
            <script type="text/javascript" src="{{ url('/public/assets') }}/js/jquery.appear.js"></script> 

            <!-- Menu --> 
            <script type="text/javascript" src="{{ url('/public/assets') }}/js/mega-menu/mega_menu.js"></script> 

            <!-- owl-carousel --> 
            <script type="text/javascript" src="{{ url('/public/assets') }}/js/owl-carousel/owl.carousel.min.js"></script> 

            <!-- counter --> 
            <script type="text/javascript" src="{{ url('/public/assets') }}/js/counter/jquery.countTo.js"></script> 

            <!-- Magnific Popup --> 
            <script type="text/javascript" src="{{ url('/public/assets') }}/js/magnific-popup/jquery.magnific-popup.min.js"></script> 

            <!-- style customizer  --> 
            <script type="text/javascript" src="{{ url('/public/assets') }}/js/style-customizer.js"></script> 

            <!-- Text Rotator --> 
            <script type="text/javascript" src="{{ url('/public/assets') }}/js/jquery.simple-text-rotator.js"></script> 

            <!-- custom --> 
            <script type="text/javascript" src="{{ url('/public/assets') }}/js/custom.js"></script>

    </body>

</html>
@include('front.layout.footer')