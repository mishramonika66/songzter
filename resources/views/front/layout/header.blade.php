<header id="header" class="defualt">
    <div class="topbar">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="topbar-left text-left">
                        <ul>
                            <li><a href="mailto:support@Songzter.com"><i class="fa fa-envelope-o"> </i> support@Songzter.com </a></li>
                            <li><a href="tel:(007)1234567890"><i class="fa fa-phone"></i> (007) 123 456 7890 </a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="topbar-right text-right">
                        <ul class="list-inline social-icons color-hover">
                            <li class="social-facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li class="social-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li class="social-instagram"><a href="#"><i class="fa fa-instagram"></i></a></li>
                            <li class="social-dribbble"><a href="#"><i class="fa fa-dribbble"></i></a></li>
                        </ul>
                        <ul class="list-inline text-uppercase top-menu">
                            <li><a href="register.html">register</a></li>
                            <li><a href="login.html">login</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--=================================
   mega menu -->

    <div class="menu"> 
        <!-- menu start -->
        <nav id="menu" class="mega-menu"> 
            <!-- menu list items container -->
            <section class="menu-list-items">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12"> 
                            <!-- menu logo -->
                            <ul class="menu-logo">
                                <li> <a href="index-default.html"><img  src="{{url('/public/assets')}}/images/logo.png" alt="logo" class="logo-set"  /> </a> </li>
                            </ul>
                            <!-- menu links -->
                            <ul class="menu-links">
                                <!-- active class -->
                                <li class="active"><a href="index.html"> Home </a> </li>
                                <li><a href="feeds.html">Feeds </a></li>
                                <li><a href="feeds.html">Find people </a></li>
                                <li><a href="feeds.html">Plan</a></li>
                                <li><a href="feeds.html">Faq </a></li>
                                <li><a href="feeds.html">Contact</a></li>
                                <li><a href="feeds.html">About</a></li>
                                <li><a  onclick="openNav()">Account </a></li>				
                            </ul>
                          
                            <script>
                                function openNav() {
                                    document.getElementById("mySidenav").style.width = "250px";
                                }

                                function closeNav() {
                                    document.getElementById("mySidenav").style.width = "0";
                                }
                            </script>



                        </div>
                    </div>
                </div>
            </section>
        </nav>
        <!-- menu end --> 
    </div>
</header>