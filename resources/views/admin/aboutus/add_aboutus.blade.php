@extends('admin.layouts.layout')
@section('title', 'Admin About Us')
<!--/ in case you want to write JS, write here/-->
@section('custom_js')

<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>

<script>
    CKEDITOR.replace('summary-ckeditor');
</script>
@parent
<!--/ in case you want to write JS, write here/-->
<!--/ in case you want to write JS, write here/-->
@endsection


@section('custom_css')
<!-- BEGIN: Page CSS-->
<link rel="stylesheet" type="text/css" href="{{url('/app-assets/css/core/menu/menu-types/vertical-menu.css')}}">
<link rel="stylesheet" type="text/css" href="{{url('/app-assets/css/plugins/extensions/swiper.css')}}">

<!-- END: Page CSS-->

@parent
<!--/ in case you want to write CSS, write here/-->

<!--/ in case you want to write CSS, write here/-->
@endsection

@section('sidebar')
@parent
<!--/ in case you want add something to Sidebar, write here/-->



<!--/ in case you want add something to Sidebar, write here/-->
@endsection

@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h5 class="content-header-title float-left pr-1 mb-0">About Us</h5>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb p-0 mb-0">
                                <li class="breadcrumb-item"><a href="#"><i class="bx bx-home-alt"></i></a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Pages</a>
                                </li>
                                <li class="breadcrumb-item active">About Us
                                </li>
                                <li class="breadcrumb-item active"><a href="{{route('aboutUs.create')}}">Add</a>
                                </li>
                                 <li class="breadcrumb-item active"><a href="{{route('aboutUs.edit',1)}}">Edit</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <h1></h1>
            </div>
                <form method="POST" action="{{route('aboutUs.store')}}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label>Title:</label>
                        <input type="text" name="title" class="form-control" placeholder="Title" value=""/>
                        <input type="hidden" name="slug" value="aboutUs"/>
                        
                    </div>
                    <div class="form-group">
                        <label>Description:</label>
                        <textarea class="form-control" id="summary-ckeditor" name="summary-ckeditor"></textarea>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success btn-submit">Submit</button>
                    </div>
                </form>
        </div>
    </div>
</div>
<!-- END: Content-->
@endsection