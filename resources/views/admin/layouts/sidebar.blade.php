

<!-- BEGIN: Main Menu-->
<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto">
                <a class="navbar-brand" href="{{url('/admin')}}">
                    <div class="brand-logo"><img class="logo" src="{{url('/app-assets/images/logo/logo.png')}}" /></div>
                    <h2 class="brand-text mb-0">Admin</h2>
                </a>
            </li>
            <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="bx bx-x d-block d-xl-none font-medium-4 primary"></i><i class="toggle-icon bx bx-disc font-medium-4 d-none d-xl-block primary" data-ticon="bx-disc"></i></a></li>
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation" data-icon-style="lines">
            <li class=" nav-item"><a href="{{url('/admin')}}"><i class="menu-livicon" data-icon="desktop"></i><span class="menu-title" data-i18n="Dashboard">Dashboard</span></a>
            </li>
            <!-- User Management Actions.-->
            <li class=" navigation-header"><span>User Management</span>
            </li>
            <li class=" nav-item">
                <a href="{{route('users.index')}}"><i class="menu-livicon" data-icon="users"></i><span class="menu-title" data-i18n="User">User</span></a>
                <ul class="menu-content">
                    <li><a href="{{route('users.index')}}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="List">List</span></a>
                    </li>
                    <li><a href="{{route('users.show',1)}}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="View">View</span></a>
                    </li>
                    <li><a href="{{route('users.edit',1)}}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Edit">Edit</span></a>
                    </li>
                </ul>
            </li>
            <!-- All Static pages will list here -->
            <li class=" navigation-header"><span>Sales</span>
            </li>
            <li class=" nav-item">
                <a href="{{route('invoice.index')}}"><i class="menu-livicon" data-icon="coins"></i><span class="menu-title" data-i18n="Pages">Manage Sales</span></a>
                <ul class="menu-content">
                    <li><a href="{{route('invoice.index')}}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Export">Invoices</span></a>
                    </li>
                </ul>
            </li>
            <!-- All Static pages will list here -->
            <li class=" navigation-header"><span>Content Management</span>
            </li>
            <li class=" nav-item">
                <a href="{{route('aboutUs.index')}}"><i class="menu-livicon" data-icon="tablet"></i><span class="menu-title" data-i18n="Pages">Manage Pages</span></a>
                <ul class="menu-content">
                    <li><a href="{{route('aboutUs.index')}}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="About Us">Informational Pages</span></a>
                    </li>
                    <li><a href="{{route('faq.index')}}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="FAQ">FAQ</span></a>
                    </li>
                </ul>
            </li>
            <li class=" navigation-header"><span>Admin Settings</span>
            </li>
            <li class=" nav-item">
                <a href="{{route('setting.index')}}"><i class="menu-livicon" data-icon="gears"></i><span class="menu-title" data-i18n="Settings">Settings</span></a>
                <ul class="menu-content">
                    <li><a href="{{route('setting.index')}}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Settings">Site Configuration</span></a>
                    </li>
                    <li><a href="{{route('email.index')}}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Second Level">Email Configuration</span></a>
                    </li>
                    <li><a href="{{route('report.index')}}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Export">User Report</span></a>
                    </li>
                    <li><a href="{{route('notification.index')}}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Export">Push Notification</span></a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- END: Main Menu-->

