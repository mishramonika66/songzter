<!DOCTYPE html>
<html class="loading" lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <meta name="description" content="Parangat admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
        <meta name="keywords" content="">
        <meta name="author" content="Parangat Technologies">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Parangat') }} - @yield('title')</title>

        <link rel="apple-touch-icon" href="{{ url('app-assets/images/ico/apple-icon-120.png') }}">
        <link rel="shortcut icon" type="image/x-icon" href="{{ url('app-assets/images/ico/favicon.ico') }}">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,600%7CIBM+Plex+Sans:300,400,500,600,700" rel="stylesheet">

        <!-- BEGIN: Vendor CSS-->

        <link rel="stylesheet" type="text/css" href="{{url('/app-assets/vendors/css/vendors.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{url('/app-assets/vendors/css/charts/apexcharts.css')}}">
        <link rel="stylesheet" type="text/css" href="{{url('/app-assets/vendors/css/extensions/swiper.min.css')}}">

        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="{{url('app-assets/css/bootstrap.css')}}">
        <link rel="stylesheet" type="text/css" href="{{url('app-assets/css/bootstrap-extended.css')}}">
        <link rel="stylesheet" type="text/css" href="{{url('app-assets/css/colors.css')}}">
        <link rel="stylesheet" type="text/css" href="{{url('app-assets/css/components.css')}}">
        <link rel="stylesheet" type="text/css" href="{{url('app-assets/css/themes/dark-layout.css')}}">
        <link rel="stylesheet" type="text/css" href="{{url('app-assets/css/themes/semi-dark-layout.css')}}">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="{{url('app-assets/css/core/menu/menu-types/vertical-menu.css')}}">
        <link rel="stylesheet" type="text/css" href="{{url('app-assets/css/pages/dashboard-ecommerce.css')}}">

        <!-- END: Page CSS-->

        <!-- BEGIN: Custom CSS-->
        <link rel="stylesheet" type="text/css" href="{{url('assets/css/style.css')}}">

        <!-- END: Custom CSS-->


        @section('custom_css')
        <!--/ Custome CSS will be pasted here /-->
        @show

    </head>
    <body class="vertical-layout vertical-menu-modern 2-columns  navbar-sticky footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">
        <!-- ======= Header ======= -->
        <!-- BEGIN: Header-->
        @include('admin.layouts/header')
        <!-- END: Header-->
        @include('admin.layouts/sidebar')
        <!-- End Header -->

        @yield('content')
        <!--/ View    /-->


        <!-- End #main -->
        @include('admin.layouts/footer')
        <!-- BEGIN: Vendor JS-->
        <script src="{{url('/app-assets/vendors/js/vendors.min.js')}}"></script>
        <script src="{{url('/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js')}}"></script>
        <script src="{{url('/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js')}}"></script>
        <script src="{{url('/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js')}}"></script>

        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="{{url('/app-assets/vendors/js/charts/apexcharts.min.js')}}"></script>
        <script src="{{url('/app-assets/vendors/js/extensions/swiper.min.js')}}"></script>

        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="{{url('/app-assets/js/scripts/configs/vertical-menu-light.js')}}"></script>
        <script src="{{url('/app-assets/js/core/app-menu.js')}}"></script>
        <script src="{{url('/app-assets/js/core/app.js')}}"></script>
        <script src="{{url('/app-assets/js/scripts/components.js')}}"></script>
        <script src="{{url('/app-assets/js/scripts/footer.js')}}"></script>


        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script src="{{url('/app-assets/js/scripts/pages/dashboard-ecommerce.js')}}"></script>

        <!-- CK EDITOR script-->
        <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
        <script>
CKEDITOR.replace('summary-ckeditor');
        </script>
        <!-- CK Editor js end -->

        <!-- END: Page JS-->
        @section('custom_js')

        <!--/ Custome Js /-->
        @show
    </body>
</html>