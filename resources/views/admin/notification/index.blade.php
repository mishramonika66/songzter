@extends('admin.layouts.layout')
<!--/ in case you want to write JS, write here/-->

@section('custom_js')
 <script src="{{url('/app-assets/js/scripts/pages/faq.js')}}"></script>
@parent
<!--/ in case you want to write JS, write here/-->
<!--/ in case you want to write JS, write here/-->
@endsection


@section('custom_css')
<!-- BEGIN: Page CSS-->
<link rel="stylesheet" type="text/css" href="{{url('/app-assets/css/core/menu/menu-types/vertical-menu.css')}}">
<link rel="stylesheet" type="text/css" href="{{url('/app-assets/css/plugins/extensions/swiper.css')}}">
<link rel="stylesheet" type="text/css" href="{{url('/app-assets/css/pages/faq.css')}}">
<!-- END: Page CSS-->

@parent
<!--/ in case you want to write CSS, write here/-->

<!--/ in case you want to write CSS, write here/-->
@endsection

@section('sidebar')
@parent
<!--/ in case you want add something to Sidebar, write here/-->



<!--/ in case you want add something to Sidebar, write here/-->
@endsection

@section('content')


@endsection